
//variables
var c = document.getElementById("myCanvas");
var ctx = c.getContext("2d");
var centerX = c.width / 2;
var centerY = c.height / 2;
var radius = 100;
var dx = 2;
var dy = -2;
var counter;


//onload 
ctx.beginPath();
ctx.arc(centerX, centerY, radius, 0, (2 * Math.PI),false);
ctx.lineWidth = 5;
ctx.stroke();
ctx.fillStyle = "#"+ Math.floor(Math.random()*16777215).toString(16);
ctx.fill();
ctx.globalCompositeOperation = "destination-out";
ctx.globalCompositeOperation = "source-over";
ctx.closePath();

//fuction for setInterval()
function draw(){
	ctx.clearRect(0,0,c.width,c.height)
	
	ctx.beginPath();
	ctx.arc(centerX, centerY, radius, 0, (2 * Math.PI),false);
	ctx.lineWidth = 5;
	ctx.stroke();
	ctx.fill();
	ctx.globalCompositeOperation = "destination-out";
	ctx.globalCompositeOperation = "source-over";
	ctx.closePath();
	
	if(centerX + dx > c.width-radius || centerX + dx < radius) {
        dx = -dx;
    }
    if(centerY + dy > c.height-radius || centerY + dy < radius) {
        dy = -dy;
    }
    
    centerX += dx;
    centerY += dy;
}
	
//changes color
function changeColor() {
	ctx.clearRect(0,0,c.width,c.height);
	  ctx.beginPath();
ctx.arc(centerX, centerY, radius, 0, (2 * Math.PI),false);
ctx.lineWidth = 5;
ctx.stroke();
ctx.fillStyle = "#"+ Math.floor(Math.random()*16777215).toString(16);
ctx.fill();
ctx.globalCompositeOperation = "destination-out";
ctx.globalCompositeOperation = "source-over";
ctx.closePath();
}

//make ball smaller
function smaller() {
  if (radius == 100){
	  radius = 50;
	  ctx.clearRect(0,0,c.width,c.height);
	  ctx.beginPath();
ctx.arc(centerX, centerY, radius, 0, (2 * Math.PI),false);
ctx.lineWidth = 5;
ctx.stroke();
ctx.fill();
ctx.globalCompositeOperation = "destination-out";
ctx.globalCompositeOperation = "source-over";
ctx.closePath();
  }
	else{
		return false;
	}
}

// makes ball larger
function larger() {
	if (radius == 50){
	  radius = 100;
	  ctx.clearRect(0,0,c.width,c.height);
	  ctx.beginPath();
ctx.arc(centerX, centerY, radius, 0, (2 * Math.PI),false);
ctx.lineWidth = 5;
ctx.stroke();
ctx.fill();
ctx.globalCompositeOperation = "destination-out";
ctx.globalCompositeOperation = "source-over";
ctx.closePath();
  }
	else{
		return false;
	}
}

// start/stops ball
function bounce() {
	if (!counter){
		counter = setInterval(draw,20);
	}
	else{
		clearInterval(counter);
		counter = null;
	}
}
